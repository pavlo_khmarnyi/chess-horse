package sample.view;

import java.util.List;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import sample.model.Point;

public interface MainView {
    void enableStartButton();
    void disableStartButton();
    void enableLaunchButton();
    void disableLaunchButton();
    void setCellColor(Rectangle rectangle, Color color);
    void setCellColor(Rectangle rectangle, int x, int y);
    void showKnightTrack(List<Point> track);
    void enableChessBoard();
    void disableChessBoard();
    void clearChessBoard();
}
