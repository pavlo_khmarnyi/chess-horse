package sample;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import sample.model.Point;
import sample.presenter.MainPresenter;
import sample.presenter.impl.MainPresenterImpl;
import sample.view.MainView;

public class Main extends Application implements MainView {
    private MainPresenter presenter;

    private GridPane boardGridPane;
    private Button startButton;
    private Button launchButton;

    private Point startPoint;
    private Point finishPoint;

    private Rectangle[][] chessBoard = new Rectangle[8][8];

    @Override
    public void start(Stage primaryStage) throws Exception {
        presenter = new MainPresenterImpl(this);

        HBox hBox = new HBox();
        List<TextArea> textAreas = new ArrayList<>();
        textAreas.add(new TextArea(""));
        textAreas.add(new TextArea("A"));
        textAreas.add(new TextArea("B"));
        textAreas.add(new TextArea("C"));
        textAreas.add(new TextArea("D"));
        textAreas.add(new TextArea("E"));
        textAreas.add(new TextArea("F"));
        textAreas.add(new TextArea("G"));
        textAreas.add(new TextArea("H"));
        for (TextArea textArea : textAreas) {
            textArea.setPrefSize(50, 50);
            textArea.setEditable(false);
            hBox.getChildren().add(textArea);
        }

        VBox vBox = new VBox();
        for (int i = 0; i < 8; i++) {
            TextArea textArea = new TextArea(i + "");
            textArea.setPrefSize(50, 50);
            textArea.setEditable(false);
            vBox.getChildren().add(textArea);
        }

        boardGridPane = new GridPane();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                int y = i;
                int x = j;
                Rectangle rectangle = new Rectangle(50, 50);
                rectangle.setOnMouseClicked(event -> {
                    Rectangle cell = (Rectangle) event.getTarget();
                    if (startPoint == null) {
                        startPoint = new Point(x, y);
                        setCellColor(cell, Color.RED);
                    } else if (finishPoint == null) {
                        finishPoint = new Point(x, y);
                        setCellColor(cell, Color.BLUE);
                        enableLaunchButton();
                        disableChessBoard();
                    }
                    System.out.println("x = " + x + " y = " + y);
                });
                setCellColor(rectangle, i, j);
                chessBoard[x][y] = rectangle;
                boardGridPane.add(rectangle, j * 50, i * 50);
            }
        }

        startButton = new Button("New game");
        startButton.setPrefSize(225, 60);
        startButton.setDisable(true);
        startButton.setOnMouseClicked(event ->
                presenter.clearAll()
        );

        launchButton = new Button("Launch");
        launchButton.setPrefSize(225, 60);
        launchButton.setDisable(true);
        launchButton.setOnMouseClicked(event ->
                presenter.solvePath(startPoint, finishPoint)
        );

        HBox buttonBox = new HBox();
        buttonBox.getChildren().addAll(startButton, launchButton);

        BorderPane borderPane = new BorderPane();
        borderPane.setTop(hBox);
        borderPane.setLeft(vBox);
        borderPane.setCenter(boardGridPane);
        borderPane.setBottom(buttonBox);

        primaryStage.setTitle("Chess");
        primaryStage.setScene(new Scene(borderPane, 450, 510));
        primaryStage.setMaximized(false);
        primaryStage.setResizable(false);
        primaryStage.sizeToScene();
        primaryStage.show();
    }

    @Override
    public void enableStartButton() {
        if (startButton.isDisabled()) {
            startButton.setDisable(false);
        }
    }

    @Override
    public void disableStartButton() {
        if (!startButton.isDisabled()) {
            startButton.setDisable(true);
        }
    }

    @Override
    public void enableLaunchButton() {
        if (launchButton.isDisabled()) {
            launchButton.setDisable(false);
        }
    }

    @Override
    public void disableLaunchButton() {
        if (!launchButton.isDisabled()) {
            launchButton.setDisable(true);
        }
    }

    @Override
    public void setCellColor(Rectangle rectangle, Color color) {
        rectangle.setFill(color);
    }

    @Override
    public void setCellColor(Rectangle rectangle, int x, int y) {
        Color color = (x % 2 != 0 && y % 2 == 0) || (x % 2 == 0 && y % 2 != 0)
                ? Color.BLACK
                : Color.TRANSPARENT;
        rectangle.setFill(color);
    }

    @Override
    public void showKnightTrack(List<Point> track) {
        for (Point point : track) {
            if (point.equals(startPoint) || point.equals(finishPoint)) {
                continue;
            }
            setCellColor(chessBoard[point.getX()][point.getY()], Color.GREEN);
        }
    }

    @Override
    public void enableChessBoard() {
        boardGridPane.setDisable(false);
    }

    @Override
    public void disableChessBoard() {
        boardGridPane.setDisable(true);
    }

    @Override
    public void clearChessBoard() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                setCellColor(chessBoard[j][i], j, i);
            }
        }
        startPoint = null;
        finishPoint = null;
    }
}
