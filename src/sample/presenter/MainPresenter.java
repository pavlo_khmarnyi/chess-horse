package sample.presenter;

import sample.model.Point;

public interface MainPresenter {
    void solvePath(Point startPoint, Point finishPoint);
    void clearAll();
}
