package sample.presenter.impl;

import java.util.List;

import sample.algorithm.KnightMovement;
import sample.model.Point;
import sample.presenter.MainPresenter;
import sample.view.MainView;

public class MainPresenterImpl implements MainPresenter {
    private MainView view;
    private KnightMovement knightMove;

    public MainPresenterImpl(MainView view) {
        this.view = view;
        this.knightMove = KnightMovement.of();
    }

    @Override
    public void solvePath(Point startPoint, Point finishPoint) {
        view.disableStartButton();
        view.disableLaunchButton();
        List<Point> track = knightMove.findTrack(startPoint, finishPoint);
        view.showKnightTrack(track);
        view.enableStartButton();
    }

    @Override
    public void clearAll() {
        view.clearChessBoard();
        view.enableChessBoard();
        view.disableStartButton();
    }
}
