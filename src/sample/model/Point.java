package sample.model;

import java.util.ArrayList;
import java.util.List;

public class Point {
    private Point parent;
    private final List<Point> children = new ArrayList<>();

    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Point(int x, int y, Point parent) {
        this(x, y);
        this.parent = parent;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Point getParent() {
        return parent;
    }

    public void setParent(Point parent) {
        this.parent = parent;
    }

    public List<Point> getChildren() {
        return children;
    }

    public boolean equals(Point point) {
        return this.x == point.getX() && this.y == point.getY();
    }
}
