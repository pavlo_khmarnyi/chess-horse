package sample.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import sample.model.Point;

public class KnightMovement {
    private Point startPoint;
    private Point finishPoint;

    private List<Point> track = new ArrayList<>();
    private List<Point> visitedPoints = new ArrayList<>();

    private static KnightMovement INSTANCE;
    private static final Point[] LEGAL_MOVES = new Point[] {
            new Point(1, -2),
            new Point(2, -1),
            new Point(2, 1),
            new Point(1, 2),
            new Point(-1, 2),
            new Point(-2, 1),
            new Point(-2, -1),
            new Point(-1, -2)
    };

    public static KnightMovement of() {
        if (INSTANCE == null) {
            INSTANCE = new KnightMovement();
        }
        return INSTANCE;
    }

    private KnightMovement() {}

    public List<Point> findTrack(Point startPoint, Point finishPoint) {
        if (visitedPoints.size() > 0) {
            visitedPoints.clear();
        }
        if (track.size() > 0) {
            track.clear();
        }
        this.startPoint = startPoint;
        this.finishPoint = finishPoint;
        move(new ArrayList<Point>() {{ add(startPoint); }});
        fillTrack(finishPoint);
        return track;
    }

    private void move(List<Point> points) {
        List<Point> newGeneration = nextGeneration(points);
        if (!checkPoints(newGeneration)) {
            move(newGeneration);
        }
    }

    private void fillTrack(Point point) {
        Point parent = point.getParent();
        if (!track.contains(parent)) {
            track.add(parent);
        }
        if (!parent.equals(startPoint)) {
            fillTrack(parent);
        }
    }

    private List<Point> findChildren(Point point) {
        Arrays.stream(LEGAL_MOVES).forEach(move -> {
            Point child = getChild(point, move);
            if (!(child == null || isVisited(child))) {
                point.getChildren().add(child);
                child.setParent(point);
            }
        });
        return point.getChildren();
    }

    private boolean checkPoint(Point point) {
        if (point.equals(finishPoint)) {
            finishPoint.setParent(point.getParent());
            return true;
        }
        return false;
    }

    private boolean checkPoints(List<Point> points) {
        return points.stream().anyMatch(point -> checkPoint(point));
    }

    private List<Point> nextGeneration(List<Point> oldGeneration) {
        List<Point> newGeneration = new ArrayList<>();
        for (Point point: oldGeneration) {
            newGeneration.addAll(findChildren(point));
        }
        return newGeneration;
    }

    private Point getChild(Point point, Point move) {
        int x = point.getX() + move.getX();
        int y = point.getY() + move.getY();
        return hasChild(point, x, y) ? new Point(x, y, point) : null;
    }

    private boolean hasChild(Point point, int x, int y) {
        return !(x > 7 || x < 0 || y > 7 || y < 0);
    }

    private boolean isVisited(Point point) {
        return visitedPoints.stream().anyMatch(visitedPoint -> point.equals(visitedPoint));
    }
}
